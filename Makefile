TARGET = abiwuthasn

CC ?= cc
CFLAGS += -std=c99 -Wall -Wextra -pedantic
DBGFLAGS = -g -DDEBUG
OPTFLAGS = -Ofast -flto

SRCDIR = src
OBJDIR = obj
DBGOBJDIR = obj-dbg

CCSOURCES = compiler.c interpreter.c main.c preprocessor.c status.c tape.c
OBJECTS = $(CCSOURCES:%.c=%.o)

default: $(TARGET)

all: $(TARGET) debug

$(TARGET): $(addprefix $(OBJDIR)/,$(OBJECTS))
	$(CC) $(CFLAGS) $(OPTFLAGS) -o $(TARGET) $(addprefix $(OBJDIR)/,$(OBJECTS))

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	mkdir -p $(dir $@)
	$(CC) $(CFLAGS) $(OPTFLAGS) -c $< -o $@

debug: $(addprefix $(DBGOBJDIR)/,$(OBJECTS))
	$(CC) $(CFLAGS) $(DBGFLAGS) -o $(TARGET)-dbg $(addprefix $(DBGOBJDIR)/,$(OBJECTS))

$(DBGOBJDIR)/%.o: $(SRCDIR)/%.c
	mkdir -p $(dir $@)
	$(CC) -o $@ $(CFLAGS) $(DBGFLAGS) -c $<

clean:
	rm -f $(TARGET) $(TARGET)-dbg
	rm -rf $(OBJDIR) $(DBGOBJDIR)

rebuild: clean default

.PHONY: clean

