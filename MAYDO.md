* Using finer exit codes for `main`.
* Reconsidering semantics of `-bc`.
* Creating a "library" (and in particular prefixing everything with
`abiwuthasn_`).
* Handling errors that could occur in `fgetc`, `fputc` etc.
* Marking the tape functions as `inline` (instead of relying on link time
optimizations to inline them).
* Adding a version of the interpreter which doesn't do bounds checks.
