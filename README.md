# `abiwuthasn`: a BF interpreter written in C

## Options

`-f`: Used to give a path to some BF source code to handle. `stdin` is used if
no such path is given.

`-b`: Enables "bang" mode, in which the preprocessor reads the source code
until it reaches the first exclamation exclamation mark and the interpreter
(if it is called) uses the rest of the file as input.

`-c`: Enables "compilation" mode, in which the BF code is converted to C code
which is printed to `stdout`. The generated C file will require the tape
utilities declared in `src/tape.h`.

`-o`: Used to give a path to a file into which the output (of the interpreted
BF code or the "compiler") should be written. `stdout` is used if no such path
is given.

## Features

The preprocessor replaces consecutive occurrences of head movement and cell
incrementation instructions by numbers representing the change made.

Each cell of the memory tape contains an `int` (by default, cf. compilation
flags below). The tape is expanded dynamically in both directions.

## Building

`abiwuthasn` can be built with GCC, Clang, and possibly other C99-compliant
compilers.

Following macros can be defined (e.g. via a compiler argument of the form
`-D<MACRO>`) to configure the binary:

* `USE_COMPUTED_GOTO`: If this macro is defined, a faster interpreter using
computed `goto`s is compiled. (This is disabled by default because computed
`goto`s are a non-standard feature, but both GCC and Clang support them.)
* `INITIAL_CODE_SIZE` (default: `128`): Initial size of the buffer which holds
the instructions.
* `CELL` (default: `int`): A signed integer type (from `stdint.h`) that is
used as the cell type for the memory tape of the interpreter.
* `DEFAULT_PAGE_SIZE` (default: `30000`): How many cells each "page" of the
tape should contain. (In particular, the tape is extended in steps of
`DEFAULT_PAGE_SIZE` when necessary.)

A makefile is available, with following configurable variables:

* `CC`: The C compiler to use `cc` by default.
* `CFLAGS`: Additional options to pass to the compiler (for example those of
the form `-D<MACRO>=<VALUE>`).

## License

All of the original work in this repository (i.e. everything except for the
files in `exp`) is dedicated to the public domain via
[CC0](http://creativecommons.org/publicdomain/zero/1.0/).
