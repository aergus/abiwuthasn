#!/bin/env python3

import statistics
import subprocess
import time

WARM_UP = 10
RUNS = 100

EXAMPLES = ["walk", "squares", "dbfi+dbfi+cat"]
EXAMPLES_DIR = "examples"
ARGS = ["./abiwuthasn", "-b", "-f"]

for example in EXAMPLES:
    path = EXAMPLES_DIR + "/"  + example + ".b"
    data = []
    for _ in range(WARM_UP):
        subprocess.run(ARGS + [path], check=True, stdout=subprocess.DEVNULL)
    for _ in range(RUNS):
        before = time.perf_counter()
        subprocess.run(ARGS + [path], check=True, stdout=subprocess.DEVNULL)
        after = time.perf_counter()
        data.append(after - before)
    mean = statistics.mean(data)
    stdev = statistics.pstdev(data)
    print("{} {} {}".format(example, mean, stdev))
