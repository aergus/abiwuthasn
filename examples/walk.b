a program that "walks through" about 4k cells
(requires "negative tape indices" and cells that can hold at least 13 bits)

by Aras Ergus
dedicated to the public domain via CC0

                        (we write ~ instead of "minus")
<<                      move to cell ~2

++++++++++++++++        set cell ~2 to 16
[                       while cell ~2 is not 0
  > ++++++++++++++++        set cell ~1 to 16
  [                         while cell ~1 is not 0
    > ++++++++++++++++          increment cell 0 by 16
    < -                         decrement cell ~1 by 1
  ]
  < -                       decrement cell ~2 cell by 1
]
                        (cell 0 is now set to 4096)
>>                      move to cell 0

[                       while the current cell is not 0
  -                         decrement the current cell
  [                         move the current cell to the next cell
    > +
    < -
  ]
  >                         move to the next cell
]
