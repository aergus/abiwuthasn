outputs square numbers from 0 to 10000

by Dainel B Cristofani (see below for more information)

available under a Creative Commons Attribution Share Alike License

++++[>+++++<-]>[<+++++>-]+<+[
    >[>+>+<<-]++>>[<<+>>-]>>>[-]++>[-]+
    >>>+[[-]++++++>>>]<<<[[<++++++++<++>>-]+<.<[>----<-]<]
    <<[>>>>>[>>>[-]+++++++++<[>-<-]+++++++++>[-[<->-]+[<<<]]<[>+<-]>]<<-]<<-
]

[Outputs square numbers from 0 to 10000.
Daniel B Cristofani (cristofdathevanetdotcom)
http://www.hevanet.com/cristofd/brainfuck/]

[According to brainfuck.org, where this program was downloaded, it is
available under a Creative Commons Attribution-ShareAlike 4.0 International
License.]
