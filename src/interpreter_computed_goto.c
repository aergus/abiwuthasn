/* interpreter_computed_goto.c -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <stdlib.h>

typedef struct instruction_cg {
  void* label;
  instruction_payload payload;
} instruction_cg;

status interpret_on(instructions code, tape tape, FILE* input, FILE* output) {
  instruction_cg* bytecode =
      (instruction_cg*)malloc((code.length + 1) * sizeof(instruction_cg));
  if (bytecode == NULL) {
    return CANNOT_INITIALIZE_BYTECODE;
  }
  instruction_cg* start = bytecode;

  size_t i = 0;
  for (i = 0; i < code.length; i++) {
    bytecode[i].payload = code.payload[i].payload;
    switch (code.payload[i].type) {
      case INCREMENT:
        bytecode[i].label = &&increment;
        break;
      case MOVE_FORWARD:
        bytecode[i].label = &&move_forward;
        break;
      case MOVE_BACKWARDS:
        bytecode[i].label = &&move_backwards;
        break;
      case OPEN_BRACKET:
        bytecode[i].label = &&open_bracket;
        break;
      case CLOSE_BRACKET:
        bytecode[i].label = &&close_bracket;
        break;
      case READ:
        bytecode[i].label = &&read;
        break;
      case WRITE:
        bytecode[i].label = &&write;
        break;
      default:
        break;
    }
  }
  bytecode[code.length].label = &&halt;

  goto * bytecode->label;

increment:
  increment_cell(tape, bytecode->payload.incrementation);
  bytecode++;
  goto * bytecode->label;
move_forward:
  if (!move_head_forward(&tape, bytecode->payload.movement)) {
    free(start);
    return INTERPRETER_CANNOT_EXTEND;
  }
  bytecode++;
  goto * bytecode->label;
move_backwards:
  if (!move_head_backwards(&tape, bytecode->payload.movement)) {
    free(start);
    return INTERPRETER_CANNOT_EXTEND;
  }
  bytecode++;
  goto * bytecode->label;
open_bracket:
  if (!get_cell(tape)) {
    bytecode += bytecode->payload.movement;
  }
  bytecode++;
  goto * bytecode->label;
close_bracket:
  if (get_cell(tape)) {
    bytecode += bytecode->payload.movement;
  }
  bytecode++;
  goto * bytecode->label;
read:
  set_cell(tape, fgetc(input));
  bytecode++;
  goto * bytecode->label;
write:
  fputc(get_cell(tape), output);
  bytecode++;
  goto * bytecode->label;
halt:
  free(start);
  return SUCCESS;
}
