/* main.c -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include <getopt.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "compiler.h"
#include "interpreter.h"
#include "preprocessor.h"

#define PROGRAM_NAME "abiwuthasn"

int report(status status) {
  if (status == SUCCESS) {
    return 0;
  } else {
    fprintf(stderr, "%s: %s\n", PROGRAM_NAME, status_strings[status]);
    return 1;
  }
}

int main(int argc, char** argv) {
  FILE* source = stdin;
  FILE* target = stdout;
  bool bang_flag = false;
  bool compile_flag = 0;
  int c = 0;
  while ((c = getopt(argc, argv, "bcf:o:")) != -1) {
    switch (c) {
      case 'b':
        bang_flag = true;
        break;
      case 'c':
        compile_flag = true;
        break;
      case 'f':
        source = fopen(optarg, "r");
        if (source == NULL) {
          fprintf(stderr, "%s: could not open input file '%s'\n", PROGRAM_NAME,
                  optarg);
          if (target != NULL) {
            fclose(target);
          }
          return 1;
        }
        break;
      case 'o':
        /* Don't want to copy a string, so opening the output file already here
           even though it could wait until the code is preprocessed.
           (Hopefully, the time the preprocessor needs will be negligible in
           most cases.)
         */
        target = fopen(optarg, "w");
        if (target == NULL) {
          fprintf(stderr, "%s: could not open output file '%s'\n", PROGRAM_NAME,
                  optarg);
          if (source != NULL) {
            fclose(source);
          }
          return 1;
        }
        break;
      default:
        break;
    }
  }

  instructions instructions = {.payload = NULL, .length = 0};
  status status = preprocess_file(source, bang_flag, &instructions);
  /* Given the current implementation of `preprocess_file`, the first condition
     is redundant, but this looks nicer.
   */
  if (status != SUCCESS || instructions.length == 0) {
    fclose(source);
    fclose(target);
    return report(status);
  }

  if (compile_flag) {
    fclose(source);
    write_c_code(instructions, target);
    fclose(target);
    free(instructions.payload);
    return report(SUCCESS);
  } else {
    if (bang_flag) {
      status = interpret(instructions, source, target);
      fclose(source);
    } else {
      if (source != stdin) {
        fclose(source);
      }
      status = interpret(instructions, stdin, target);
      /* We don't bother to close `stdin`. */
    }
    fclose(target);
    free(instructions.payload);
    return report(status);
  }
}
