/* instruction.h -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _INSTRUCTION__H
#define _INSTRUCTION__H

#include <stddef.h>

#include "tape.h"

typedef enum instruction_type {
  INCREMENT,
  /* Indicates that the `movement` payload is positive. */
  MOVE_FORWARD,
  /* Indicates that the `movement` payload is negative. */
  /* Storing a positive value which is then _subtracted_ from the tape index
     was considered, but that turned out to even worsen the performance in
     some benchmarks.
   */
  MOVE_BACKWARDS,
  OPEN_BRACKET,
  CLOSE_BRACKET,
  READ,
  WRITE
} instruction_type;

typedef union instruction_payload {
  CELL incrementation;
  ptrdiff_t movement;
} instruction_payload;

typedef struct instruction {
  instruction_payload payload;
  instruction_type type;
} instruction;

#define INCREMENT_INSTRUCTION(n) \
  ((instruction){.type = INCREMENT, .payload = {.incrementation = (n)}})
#define MOVE_FORWARD_INSTRUCTION(n) \
  ((instruction){.type = MOVE_FORWARD, .payload = {.movement = (n)}})
#define MOVE_BACKWARDS_INSTRUCTION(n) \
  ((instruction){.type = MOVE_BACKWARDS, .payload = {.movement = (n)}})
#define OPEN_BRACKET_INSTRUCTION(n) \
  ((instruction){.type = OPEN_BRACKET, .payload = {.movement = (n)}})
#define CLOSE_BRACKET_INSTRUCTION(n) \
  ((instruction){.type = CLOSE_BRACKET, .payload = {.movement = (n)}})
#define READ_INSTRUCTION ((instruction){.type = READ})
#define WRITE_INSTRUCTION ((instruction){.type = WRITE})

typedef struct instructions {
  instruction* payload;
  size_t length;
} instructions;

#endif
