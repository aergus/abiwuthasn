/* preprocessor.h -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _PREPROCESSOR__H
#define _PREPROCESSOR__H

#include <stdbool.h>
#include <stdio.h>

#include "instruction.h"
#include "status.h"

#ifndef INITIAL_CODE_SIZE
#define INITIAL_CODE_SIZE 128
#endif

status preprocess_file(FILE* source, bool bang_flag, instructions* target);

#endif
