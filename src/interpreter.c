/* interpreter.c -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "interpreter.h"

status interpret(instructions code, FILE* input, FILE* output) {
  tape tape = new_tape(DEFAULT_PAGE_SIZE);
  if (tape.page == NULL) {
    return INTERPRETER_CANNOT_INITIALIZE;
  }
  status status = interpret_on(code, tape, input, output);
  free_pages(tape.page);
  return status;
}

#ifdef USE_COMPUTED_GOTO
#include "interpreter_computed_goto.c"
#else
#include "interpreter_switch.c"
#endif
