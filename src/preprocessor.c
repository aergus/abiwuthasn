/* preprocessor.c -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "preprocessor.h"

#include <stdlib.h>

/* This horrible macro is here because we have to use basically the same code
   four times, but with different instruction types which may require different
   types for `count`.
 */
#define ACCUMULATE_INSTRUCTIONS(instruction_char, opposite_instruction_char, \
                                count, source, bang_flag)                    \
  count = 1;                                                                 \
  while (1) {                                                                \
    int c = fgetc(source);                                                   \
    if (c == instruction_char) {                                             \
      count++;                                                               \
    } else if (c == opposite_instruction_char) {                             \
      count--;                                                               \
    } else if (c == '+' || c == '-' || c == '>' || c == '<' || c == '[' ||   \
               c == ']' || c == ',' || c == '.' || c == EOF ||               \
               (bang_flag && c == '!')) {                                    \
      ungetc(c, source);                                                     \
      break;                                                                 \
    }                                                                        \
  }

status preprocess_file(FILE* source, bool bang_flag, instructions* target) {
  size_t code_size = INITIAL_CODE_SIZE;
  size_t code_length = 0;
  instruction* instructions =
      (instruction*)malloc(code_size * sizeof(instruction));
  if (instructions == NULL) {
    return PREPROCESSOR_CANNOT_INITIALIZE;
  }

  size_t elevator_size = INITIAL_CODE_SIZE < 2 ? 1 : (INITIAL_CODE_SIZE / 2);
  size_t elevator_length = 0;
  size_t* levels = (size_t*)malloc(elevator_size * sizeof(size_t));
  if (levels == NULL) {
    free(instructions);
    return PREPROCESSOR_CANNOT_INITIALIZE;
  }

  CELL incrementation = 0;
  ptrdiff_t movement = 0;
  ptrdiff_t jump = 0;
  int c = 0;
  while ((c = fgetc(source)) != EOF && (!bang_flag || c != '!')) {
    /* This way, an unnecessary character may cause a reallocation,
     * but I think this is still better than doing these within the
     * cases.
     */
    if (code_length >= code_size) {
      code_size *= 2;
      instruction* new =
          (instruction*)realloc(instructions, code_size * sizeof(instruction));
      if (new == NULL) {
        free(instructions);
        free(levels);
        return PREPROCESSOR_CANNOT_EXTEND;
      }
      instructions = new;
    }

    if (elevator_length >= elevator_size) {
      elevator_size *= 2;
      size_t* new = (size_t*)realloc(levels, elevator_size * sizeof(size_t));
      if (new == NULL) {
        free(instructions);
        free(levels);
        return PREPROCESSOR_CANNOT_EXTEND;
      }
      levels = new;
    }

    switch (c) {
      case '+':
        ACCUMULATE_INSTRUCTIONS('+', '-', incrementation, source, bang_flag);
        if (incrementation != 0) {
          instructions[code_length] = INCREMENT_INSTRUCTION(incrementation);
          code_length++;
        }
        break;
      case '-':
        ACCUMULATE_INSTRUCTIONS('-', '+', incrementation, source, bang_flag);
        if (incrementation != 0) {
          instructions[code_length] = INCREMENT_INSTRUCTION(-incrementation);
          code_length++;
        }
        break;
      case '>':
        ACCUMULATE_INSTRUCTIONS('>', '<', movement, source, bang_flag);
        if (movement > 0) {
          instructions[code_length] = MOVE_FORWARD_INSTRUCTION(movement);
          code_length++;
        } else if (movement < 0) {
          instructions[code_length] = MOVE_BACKWARDS_INSTRUCTION(movement);
          code_length++;
        }
        break;
      case '<':
        ACCUMULATE_INSTRUCTIONS('<', '>', movement, source, bang_flag);
        if (movement > 0) {
          instructions[code_length] = MOVE_BACKWARDS_INSTRUCTION(-movement);
          code_length++;
        } else if (movement < 0) {
          instructions[code_length] = MOVE_FORWARD_INSTRUCTION(-movement);
          code_length++;
        }
        break;
      case '[':
        levels[elevator_length] = code_length;
        elevator_length++;
        /* We will add an appropriate OPENING_BRACKET_INSTRUCTION to the
           instructions when we process the matching closing bracket.
         */
        code_length++;
        break;
      case ']':
        if (elevator_length < 1) {
          free(instructions);
          free(levels);
          return UNOPENED_BRACKET;
        }
        jump = code_length - levels[elevator_length - 1];
        instructions[levels[elevator_length - 1]] =
            OPEN_BRACKET_INSTRUCTION(jump);
        elevator_length--;
        instructions[code_length] = CLOSE_BRACKET_INSTRUCTION(-jump);
        code_length++;
        break;
      case ',':
        instructions[code_length] = READ_INSTRUCTION;
        code_length++;
        break;
      case '.':
        instructions[code_length] = WRITE_INSTRUCTION;
        code_length++;
        break;
      default:
        break;
    }
  }

  free(levels);
  if (elevator_length > 0) {
    free(instructions);
    return UNCLOSED_BRACKET;
  }

  instruction* new =
      (instruction*)realloc(instructions, code_length * sizeof(instruction));
  if (new != NULL) {
    instructions = new;
  }

  target->payload = instructions;
  target->length = code_length;
  return SUCCESS;
}
