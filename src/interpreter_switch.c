/* interpreter_switch.c -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

status interpret_on(instructions code, tape tape, FILE* input, FILE* output) {
  size_t i = 0;
  for (i = 0; i < code.length; i++) {
    switch (code.payload[i].type) {
      case INCREMENT:
        increment_cell(tape, code.payload[i].payload.incrementation);
        break;
      case MOVE_FORWARD:
        if (!move_head_forward(&tape, code.payload[i].payload.movement)) {
          return INTERPRETER_CANNOT_EXTEND;
        }
        break;
      case MOVE_BACKWARDS:
        if (!move_head_backwards(&tape, code.payload[i].payload.movement)) {
          return INTERPRETER_CANNOT_EXTEND;
        }
        break;
      case OPEN_BRACKET:
        if (!get_cell(tape)) {
          i += code.payload[i].payload.movement;
        }
        break;
      case CLOSE_BRACKET:
        if (get_cell(tape)) {
          i += code.payload[i].payload.movement;
        }
        break;
      case READ:
        set_cell(tape, fgetc(input));
        break;
      case WRITE:
        fputc(get_cell(tape), output);
        break;
      default:
        break;
    }
  }
  return SUCCESS;
}
