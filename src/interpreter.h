/* interpreter.h -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _INTERPRETER__H
#define _INTERPRETER__H

#include <stdio.h>

#include "instruction.h"
#include "status.h"
#include "tape.h"

status interpret(instructions code, FILE* input, FILE* output);
status interpret_on(instructions code, tape tape, FILE* input, FILE* output);

#endif
