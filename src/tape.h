/* tape.h -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _TAPE__H
#define _TAPE__H

#include <stdbool.h>
#include <stddef.h>

#ifndef CELL
#define CELL int
#else
/* We might need some integer types that are normally not in scope... */
#include <stdint.h>
#endif

#ifndef DEFAULT_PAGE_SIZE
#define DEFAULT_PAGE_SIZE 30000
#endif

typedef struct tape_page {
  CELL* payload;
  struct tape_page* previous;
  struct tape_page* next;
} tape_page;

typedef struct tape {
  size_t head;
  size_t page_size;
  tape_page* page;
} tape;

tape_page* new_page(size_t page_size);
tape new_tape(size_t page_size);

void free_pages(tape_page* page);

CELL get_cell(tape tape);
void set_cell(tape tape, CELL value);
void increment_cell(tape tape, CELL incrementation);
bool move_head_forward(tape* tape, ptrdiff_t movement);
bool move_head_backwards(tape* tape, ptrdiff_t movement);
bool move_head(tape* tape, ptrdiff_t movement);

#endif
