/* status.c -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "status.h"

#define X(code, message) message
char* status_strings[] = {
#include "status_table.h"
};
#undef X
