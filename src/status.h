/* status.h -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#ifndef _STATUS__H
#define _STATUS__H

#define X(code, message) code
typedef enum status {
#include "status_table.h"
} status;
#undef X

extern char* status_strings[];

#endif
