/* status_table.h -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

X(SUCCESS, ""), X(UNOPENED_BRACKET, "unopened bracket"),
    X(UNCLOSED_BRACKET, "unclosed bracket"),
    X(PREPROCESSOR_CANNOT_INITIALIZE,
      "preprocessor failed to initialize a buffer"),
    X(PREPROCESSOR_CANNOT_EXTEND, "preprocessor failed to extend a buffer"),
    X(INTERPRETER_CANNOT_INITIALIZE,
      "interpreter failed to initialize the tape"),
    X(INTERPRETER_CANNOT_EXTEND, "interpreter failed to extend the tape"),
    X(CANNOT_INITIALIZE_BYTECODE,
      "interpreter failed to initialize the bytecode buffer")
