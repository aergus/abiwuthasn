/* tape.c -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "tape.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

tape_page* new_page(size_t page_size) {
  void* ptr = malloc(sizeof(tape_page) + page_size * sizeof(CELL));
  if (ptr == NULL) {
    return NULL;
  }

  tape_page* page = (tape_page*)ptr;
  page->payload = (CELL*)(page + 1);
  memset(page->payload, 0, page_size * sizeof(CELL));
  page->previous = NULL;
  page->next = NULL;
  return page;
}

tape new_tape(size_t page_size) {
  tape tape = {.head = 0, .page_size = page_size, .page = new_page(page_size)};
  return tape;
}

void free_pages(tape_page* page) {
  tape_page* current = page;
  tape_page* next = current->next;
  while (current != NULL) {
    tape_page* previous = current->previous;
    free(current);
    current = previous;
  }
  if (next != NULL) {
    current = next;
    while (current != NULL) {
      next = current->next;
      free(current);
      current = next;
    }
  }
}

CELL get_cell(tape tape) {
  return tape.page->payload[tape.head];
}

void set_cell(tape tape, CELL value) {
  tape.page->payload[tape.head] = value;
}

void increment_cell(tape tape, CELL incrementation) {
  tape.page->payload[tape.head] += incrementation;
}

bool move_head_forward(tape* tape, ptrdiff_t movement) {
  ptrdiff_t diff = movement;

  while (((size_t)diff >= tape->page_size ||
          tape->head >= tape->page_size - diff)) {
    if (tape->page->next != NULL) {
      tape->page = tape->page->next;
    } else {
      tape_page* page = new_page(tape->page_size);
      if (page == NULL) {
        return false;
      }
      page->previous = tape->page;
      tape->page->next = page;
      tape->page = page;
    }
    /* Assume a `tape->head = 0` here. */
    diff -= tape->page_size - tape->head;
    if (tape->page_size > (size_t)diff) {
      tape->head = diff;
      diff = 0;
    } else {
      tape->head = tape->page_size - 1;
      diff -= tape->page_size - 1;
    }
  }

  tape->head += diff;
  return true;
}

bool move_head_backwards(tape* tape, ptrdiff_t movement) {
  ptrdiff_t diff = movement;

  while (tape->head < (size_t)-diff) {
    if (tape->page->previous != NULL) {
      tape->page = tape->page->previous;
    } else {
      tape_page* page = new_page(tape->page_size);
      if (page == NULL) {
        return false;
      }
      page->next = tape->page;
      tape->page->previous = page;
      tape->page = page;
    }
    /* Assume a `tape->head = tape->page_size - 1` here. */
    diff += tape->head + 1;
    if (tape->page_size > (size_t)-diff) {
      tape->head = tape->page_size - 1 + diff;
      diff = 0;
    } else {
      tape->head = 0;
      diff += tape->page_size - 1;
    }
  }

  tape->head += diff;
  return true;
}

bool move_head(tape* tape, ptrdiff_t movement) {
  if (movement >= 0) {
    return move_head_forward(tape, movement);
  } else {
    return move_head_backwards(tape, movement);
  }
}
