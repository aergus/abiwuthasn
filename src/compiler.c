/* compiler.c -- part of abiwuthasn, a BF interpreter
 *
 * Written in 2012-2021 by Aras Ergus <arasergus@posteo.net>
 *
 * To the extent possible under law, the author(s) have dedicated all copyright
 * and related and neighboring rights to this software to the public domain
 * worldwide by associating CC0 with it. This software is distributed without
 * any warranty.
 *
 * For more information, please see
 * <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

#include "compiler.h"

#include <stdint.h>

void write_c_code(instructions code, FILE* target) {
  size_t i = 0, j = 0, depth = 1;
  fprintf(target, "#include <stdio.h>\n\n");
  fprintf(target, "#include \"tape.h\"\n\n");
  fprintf(target, "int main () {\n");
  fprintf(target, "  tape tape = new_tape(%i);\n", DEFAULT_PAGE_SIZE);
  fprintf(target, "  if (tape.page == NULL) { return 1; }\n\n");
  for (i = 0; i < code.length; i++) {
    if (code.payload[i].type == CLOSE_BRACKET) {
      depth--;
    }
    for (j = 0; j < depth; j++) {
      fprintf(target, "  ");
    }
    switch (code.payload[i].type) {
      case INCREMENT:
        fprintf(target, "increment_cell(tape, %ji);\n",
                (intmax_t)code.payload[i].payload.incrementation);
        break;
      case MOVE_FORWARD:
        fprintf(target,
                "if(!move_head_forward(&tape, %li)) {"
                " free_pages(tape.page); return 1; "
                "};\n",
                code.payload[i].payload.movement);
        break;
      case MOVE_BACKWARDS:
        fprintf(target,
                "if(!move_head_backwards(&tape, %li)) {"
                " free_pages(tape.page); return 1; "
                "};\n",
                code.payload[i].payload.movement);
        break;
      case OPEN_BRACKET:
        fprintf(target, "while (get_cell(tape)) {\n");
        depth++;
        break;
      case CLOSE_BRACKET:
        fprintf(target, "}\n");
        break;
      case READ:
        fprintf(target, "set_cell(tape, getchar());\n");
        break;
      case WRITE:
        fprintf(target, "putchar(get_cell(tape));\n");
        break;
      default:
        break;
    }
  }
  fputc('\n', target);
  fprintf(target, "  free_pages(tape.page);\n");
  fprintf(target, "  return 0;\n");
  fprintf(target, "}\n");
}
